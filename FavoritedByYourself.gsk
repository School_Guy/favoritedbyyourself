#*******************************************
# MacVersion = 1.2-beta
# MacDescription = Macro to show the user which caches he granted a favorite.
# MacAuthor = matrixfueller
# MacFileName = FavoritedByYourself.gsk
# MacUrl = https://gsak.net/board/index.php?showtopic=35192
#*******************************************

# Use explicit variable declaration
Option Explicit=Yes

GOSUB name=declareVariables
MacSettings Type=R FileCheck=N
GOSUB name=addCustomColumn
GOSUB name=changeLanguage
GOSUB name=showForm

##### Functions

BEGINSUB name="declareVariables"
	# General Variables
	Declare Var=$i Type=Numeric
	Declare Var=$Result Type=String
	Declare Var=$list Type=String
	Declare Var=$msg Type=String
	Declare Var=$count Type=Numeric
	Declare Var=$code Type=String

	$i=0
	$Result = ""
	$list = ""
	$msg = ""
	$count = 0

	# Form Variables
	Declare Var=$form Type=String
	Declare Var=$tempForm Type=String
	Declare Var=$ComboboxLanguage Type=String
	Declare Var=$ComboboxDatabase Type=String
	Declare Var=$formLabelHeader Type=String
	Declare Var=$formLabelDatabase Type=String
	Declare Var=$formLabelLanguage Type=String
	Declare Var=$formButtonSaveSettings Type=String
	Declare Var=$formButtonStart Type=String
	Declare Var=$formLabelHeader_de Type=String
	Declare Var=$formLabelDatabase_de Type=String
	Declare Var=$formLabelLanguage_de Type=String
	Declare Var=$formButtonSaveSettings_de Type=String
	Declare Var=$formButtonStart_de Type=String
	Declare Var=$formLabelHeader_en Type=String
	Declare Var=$formLabelDatabase_en Type=String
	Declare Var=$formLabelLanguage_en Type=String
	Declare Var=$formButtonSaveSettings_en Type=String
	Declare Var=$formButtonStart_en Type=String

	$formLabelHeader_de = "Von dir selbst favorisierte Caches"
	$formLabelDatabase_de = "Datenbank:"
	$formLabelLanguage_de = "Sprache:"
	$formButtonSaveSettings_de = "Einstellungen speichern"
	$formButtonStart_de = "Start"
	$formLabelHeader_en = "Caches Favourited by Yourself"
	$formLabelDatabase_en = "Database:"
	$formLabelLanguage_en = "Language:"
	$formButtonSaveSettings_en = "Save settings"
	$formButtonStart_en = "Start"
	
	# Translation Variables
	Declare Var=$result1 Type=String
	Declare Var=$result2 Type=String
	Declare Var=$status1 Type=String
	Declare Var=$status2 Type=String
	Declare Var=$status3 Type=String
	Declare Var=$messageAbort Type=String
	Declare Var=$result1_de Type=String
	Declare Var=$result2_de Type=String
	Declare Var=$status1_de Type=String
	Declare Var=$status2_de Type=String
	Declare Var=$status3_de Type=String
	Declare Var=$messageAbort_de Type=String
	Declare Var=$result1_en Type=String
	Declare Var=$result2_en Type=String
	Declare Var=$status1_en Type=String
	Declare Var=$status2_en Type=String
	Declare Var=$status3_en Type=String
	Declare Var=$messageAbort_en Type=String

	$result1_de = "Es wurden "
	$result2_de = " Caches bearbeitet!"
	$status1_de = "Cache "
	$status2_de = " von "
	$status3_de = " wird geaendert."
	$messageAbort_de = "Makro abgebrochen!"
	$result1_en = ""
	$result2_en = " Caches were processed!"
	$status1_en = "Cache "
	$status2_en = " of "
	$status3_en = " is being processed!"
	$messageAbort_en = "Macro aborted!"
ENDSUB

BEGINSUB name="showForm"
	$form = EditForm($form,"ComboboxDatabase","Values",sysinfo("databases"))
	WHILE TRUE
		$tempForm = Form($form,"") 
		BEGINCASE
		CASE $tempForm = "SystemExit" 
			MsgOk msg=$messageAbort
			BREAK
		CASE $tempForm = "ComboboxLanguage"
			GOSUB name="changeLanguage"
		CASE $tempForm = "ButtonSaveSettings"
			MacSettings Type=S Vars=ComboboxLanguage,ComboboxDatabase
		CASE $tempForm = "ButtonStart"
			GOSUB name=runMacro
			BREAK
		ENDCASE
	ENDWHILE
ENDSUB

BEGINSUB name="runMacro"
	GOSUB name=getGcApiData
	GOSUB name=prepareData
	GOSUB name=loopDatabase
ENDSUB

BEGINSUB name="addCustomColumn"
	IF (CustomConfig("Get","FavoritedByUser") = "")
		$Result = CustomConfig("Add","FavoritedByUser,Boolean,Global,,0")
	ENDIF
ENDSUB

BEGINSUB name="getGcApiData"
	$Result = gcapi2("GetCachesFavoritedByUser")
	# If error running api, so show the error message
	IF $_GcApiError
		MsgOk msg=$Result
	ELSE
		$Result = sqlite("sql","select data from GcApi where g_Contains('a:CacheCode',key)","Headings=N")
	ENDIF
ENDSUB

BEGINSUB name="prepareData"
	$Result = RegExReplace("\r\n",$Result,";")
	$list = List("List","Create",";")
	$list = List("List","Replace",$Result)
	$count = Val(List("List","Count",""))
ENDSUB

BEGINSUB name="loopDatabase"
	Transaction Action=Begin
	WHILE $i<=$count
		$msg = $status1 + NumToStr($i) + $status2 + NumToStr($count) + $status3
		ShowStatus Msg=$msg 
		$code = List("List","Item","$i")
		$Result = CustomPut("FavoritedByUser","1","$code")
		$i = $i + 1
	ENDWHILE
	Transaction Action=End

	$msg = $result1 + NumToStr($count) + $result2
	MsgOk Msg=$msg
ENDSUB

BEGINSUB name="changeLanguage"
	BEGINCASE
	CASE $ComboboxLanguage = "English"
		GOSUB name=changeLanguageToEnglish
	CASE $ComboboxLanguage = "Deutsch"
		GOSUB name=changeLanguageToGerman
	OTHERWISE
		GOSUB name=changeLanguageToEnglish
	ENDCASE
	
	# Form adjustments
	$form = EditForm($form,"LabelDatabase","Caption",$formLabelDatabase)
	$form = EditForm($form,"LabelHeader","Caption",$formLabelHeader)
	$form = EditForm($form,"LabelLanguage","Caption",$formLabelLanguage)
	$form = EditForm($form,"ButtonStart","Caption",$formButtonStart)
	$form = EditForm($form,"ButtonSaveSettings","Caption",$formButtonSaveSettings)
ENDSUB

BEGINSUB name="changeLanguageToGerman"
	$result1 = $result1_de
	$result2 = $result2_de
	$status1 = $status1_de
	$status2 = $status2_de
	$status3 = $status3_de
	$messageAbort = $messageAbort_de
	$formButtonSaveSettings = $formButtonSaveSettings_de
	$formButtonStart = $formButtonStart_de
	$formLabelDatabase = $formLabelDatabase_de
	$formLabelHeader = $formLabelHeader_de
	$formLabelLanguage = $formLabelLanguage_de
ENDSUB

BEGINSUB name="changeLanguageToEnglish"
	$result1 = $result1_en
	$result2 = $result2_en
	$status1 = $status1_en
	$status2 = $status2_en
	$status3 = $status3_en
	$messageAbort = $messageAbort_en
	$formButtonSaveSettings = $formButtonSaveSettings_en
	$formButtonStart = $formButtonStart_en
	$formLabelDatabase = $formLabelDatabase_en
	$formLabelHeader = $formLabelHeader_en
	$formLabelLanguage = $formLabelLanguage_en
ENDSUB

#####Forms
<Data> VarName=$form
#********************************************************************
# Form generated by GSAK form designer on So 23-Dez-2018 18:33:47
#********************************************************************

Name = FormMain
  Type = Form
  Caption = CachesFavouritedByYourself
  Height = 177
  Width = 505

Name = LabelHeader
  Type = Label
  Height = 25
  Left = 16
  Size = 15
  Top = 16
  Width = 275
  Caption = Caches Favourited by Yourself

Name = LabelDatabase
  Type = Label
  Height = 17
  Left = 16
  Top = 64
  Width = 57
  Caption = Database:

Name = ComboboxDatabase
  Type = Combobox
  Height = 21
  Left = 88
  Top = 64
  Width = 145
  Taborder = 10

Name = ButtonStart
  Type = Button
  Height = 25
  Left = 16
  Top = 104
  Width = 75
  Taborder = 11
  Caption = Run Macro

Name = LabelLanguage
  Type = Label
  Height = 17
  Left = 256
  Top = 64
  Width = 59
  Caption = Language:

Name = ButtonSaveSettings
  Type = Button
  Height = 25
  Left = 112
  Top = 104
  Width = 137
  Taborder = 12

Name = ComboboxLanguage
  Type = Combobox
  Exitonchange = Yes
  Height = 21
  Left = 336
  Top = 64
  Values = English;Deutsch
  Width = 145
  Taborder = 13

<enddata>
