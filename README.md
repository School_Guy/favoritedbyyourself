# FavoritedByYourself

The Link to the GSAK-Download-Page: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35192) .  
The Link to the GSAK-Support-Forum: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35193).

This Makro queries the GC-API a single time to get all caches which the User associated in GSAK has given his Favos
for (new term - "Likes"). Then this data is persisted in a custom column.

Learn how you can contribute from the file `CONTRIBUTING.md`.