# How to contribute to this macro?

I am not overly picky. Just make sure the linelength of non-`.gsk` files is only 120 characters and below. If you
have feature wishes just open an issue here.

The `.gsk` file should have proper intendation via tabs. Please make a comment in the code to explain what you are
doing when adding new functionallity.
